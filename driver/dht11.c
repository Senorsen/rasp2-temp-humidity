#include <linux/init.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/gpio.h>
#include <linux/interrupt.h>
#include <linux/cdev.h>
#include <linux/ioctl.h>
#include <linux/fs.h>
#include <linux/delay.h>
#include <asm/uaccess.h>

#define DRIVER_NAME "dht11"
#define DEVICE_NAME "dht11"

#define VAL 80 // threshold for lo/hi bit
#define RETRY 10
#define TIME_START 20
#define MAXCNT 10000


static dev_t dht11_devno; //设备号
static struct class *dht11_class;
static struct cdev dht11_dev;

// Temperature and humidity
int bits[40], data[5];

static struct gpio_config {
    int gpio_pin;
} config;

static int flag = 0;

static irqreturn_t irq_handler(int irq, void *dev) {
    printk(KERN_ERR "Enter irq\n");
//    if (flag)
//        gpio_set_value(config.led_num, !gpio_get_value(config.led_num));
    return IRQ_HANDLED;
}

// open 函数，应用程序调用open系统调用时会调用本函数
static int dht11_open(struct inode *inode, struct file *filp) {
    printk(KERN_INFO "dht11 open\n");
    return 0;

}

// release 函数，应用程序调用close系统调用时会调用本函数
static int dht11_release(struct inode *inode, struct file *filp) {
    if (flag) {
        free_irq(gpio_to_irq(config.gpio_pin), NULL);
        gpio_free(config.gpio_pin);
        flag = 0;
    }

    printk(KERN_INFO "dht11 release\n");
    return 0;
}

long dht11_read(void) {
    long i, j, cnt = 0;
    gpio_direction_output(config.gpio_pin, 1);
    msleep(500);
    gpio_set_value(config.gpio_pin, 0);
    msleep(TIME_START);
    gpio_set_value(config.gpio_pin, 1);
    gpio_direction_input(config.gpio_pin);

    cnt = 0;
    while (gpio_get_value(config.gpio_pin) == 1) {
        cnt++;
        if (cnt > MAXCNT) {
            printk(KERN_ERR "[%s %d]: dht11 no response (high0)\n", __func__, __LINE__);
            return 1;
        }
    }

    cnt = 0;
    while (gpio_get_value(config.gpio_pin) == 0) {
        cnt++;
        if (cnt > MAXCNT) {
            printk(KERN_ERR "[%s %d]: dht11 no response (low0)\n", __func__, __LINE__);
            return 1;
        }
    }

    cnt = 0;
    while (gpio_get_value(config.gpio_pin) == 1) {
        cnt++;
        if (cnt > MAXCNT) {
            printk(KERN_ERR "[%s %d]: dht11 no response (high1)\n", __func__, __LINE__);
            return 1;
        }
    }

    // 40bit value start
    for (i = 0; i < 40; i++) {
        // skip 50us
        while (gpio_get_value(config.gpio_pin) == 0) {
        }
        // read in loop, count higher than threshold
        cnt = 0;
        while (gpio_get_value(config.gpio_pin) == 1) {
            cnt++;
            if (cnt > MAXCNT) {
                break;
            }
        }
        if (cnt > MAXCNT) {
            break;
        }
        bits[i] = cnt;
    }

    for (i = 0; i < 5; i++) {
        data[i] = 0;
    }

    for (i = 0; i < 40; i++) {
        if (bits[i] > VAL) {
            data[i / 8] |= 1 << (8 - (i % 8) - 1);
        }
        printk(KERN_DEBUG "bits[%d] = %d (%d) \n", i, bits[i], bits[i] > VAL ? 1 : 0);
    }

    for (i = 0; i < 5; i++) {
        printk(KERN_INFO "data[%d] = %d\n", i, data[i]);
    }

    if (data[4] == (data[0] + data[1] + data[2] + data[3]) & 0xFF) {
        printk(KERN_INFO "checksum is okay\n");
        return 0;
    } else {
        printk(KERN_ERR "bad checksum\n");
        return 1;
    }
}

// ioctl 控制函数，应用程序调用ioctl系统调用时会调用本函数
static long dht11_ioctl(struct file *filp, unsigned int cmd, unsigned long arg) {
    int err = 0, retry = RETRY;
    switch (cmd) {
        case 0: // 0 表示命令号，一般都用宏定义来控制
        {
            if (copy_from_user(&config, (void *) arg, sizeof(struct gpio_config))) { // 从用户程序中获取配置数据
                printk(KERN_ERR "[%s %d] : copy_from user failed !\n", __func__, __LINE__);
                return -EFAULT;
            }
            printk(KERN_INFO "[%s %d]: Get gpio pin: %d\n ", __func__, __LINE__, config.gpio_pin);
            while (retry-- && dht11_read());
            break;
        }
        case 1: // 1: write temp and humidity to user space program
        {
            if (copy_to_user((void *) arg, data, sizeof(data))) {
                printk(KERN_ERR "[%s %d]: copy_to_user failed!\n", __func__, __LINE__);
                return -EFAULT;
            }
            break;
        }
        default:
            printk(KERN_INFO "[%s %d]:Invalid cmd", __func__, __LINE__);
            break;
    }
    return 0;
}


static struct file_operations dht11_fops = {
        .owner = THIS_MODULE,
        .open  = dht11_open,
        .release  = dht11_release,
        .unlocked_ioctl = dht11_ioctl,
};


static int __init dht11_init(void)
{
    int err;
    udelay(1000);

    printk(KERN_INFO "dht11 init \n");

    err = alloc_chrdev_region(&dht11_devno, 0, 1, DRIVER_NAME);
    if (err < 0) {
        goto err;
    }
    cdev_init(&dht11_dev, &dht11_fops);

    err = cdev_add(&dht11_dev, dht11_devno, 1);

    if (err < 0) {
        printk(KERN_ERR "[%s,%d]add cdev failed\n", __func__, __LINE__);
        goto FREE_DEVNO;
    }
    // 自动生成设备文件 在/dev目录下，文件名为DEVICE_NAME
    dht11_class = class_create(THIS_MODULE, DEVICE_NAME);
    if (IS_ERR(dht11_class)) {
        printk(KERN_ERR "[%s,%d]class create failed\n", __func__, __LINE__);
        goto DEV_FREE;
    }
    device_create(dht11_class, NULL, dht11_devno, NULL, DEVICE_NAME);

    return 0;
DEV_FREE:
    cdev_del(&dht11_dev);
FREE_DEVNO:
    unregister_chrdev_region(dht11_devno, 1);
err:
    return err;
}


static void dht11_exit(void) {
    if (flag) {
        free_irq(gpio_to_irq(config.gpio_pin), NULL);
        gpio_free(config.gpio_pin);
    }
    device_destroy(dht11_class, dht11_devno);
    class_destroy(dht11_class);
    cdev_del(&dht11_dev);
    unregister_chrdev_region(dht11_devno, 1);
    printk(KERN_INFO "dht11 exit\n");
}

module_init(dht11_init);
module_exit(dht11_exit);
MODULE_AUTHOR("Zhang Sen (Senorsen) <senorsen.zhang@gmail.com>");
MODULE_DESCRIPTION("DHT11 Temperature and humidity sensor by Group 15");
MODULE_LICENSE("GPL");
