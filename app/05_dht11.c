#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <fcntl.h>

int data[5];

struct gpio_config
{
    int gpio_pin;
};

int main(int argc, char **argv)
{
    int fd;
    struct gpio_config config;
    config.gpio_pin = 26;

    fd = open("/dev/dht11", O_RDWR);
    if (fd < 0) {
        perror("/dev/dht11");
        exit(0);
    }

    ioctl(fd, 0, &config);
    sleep(0.1);
    ioctl(fd, 1, &data);
    // Output JSON format
    printf("{\n    \"temperature\": %d,\n    \"humidity\": %d\n}\n", data[2], data[0]);

    close(fd);
    return 0;
}
